import unittest

from handler import fibonacci


class TestHandler(unittest.TestCase):
    def test_fibonacci(self):
        event = {"data": {"num": 5}}
        result = fibonacci(event, None).replace(" ", "")
        assert result == "[1,1,2,3,5]"

        event = {"data": {"num": 35}}
        result = fibonacci(event, None).replace(" ", "")
        assert result == "[1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657," \
                         "46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465]"
