import json


def fibonacci(event, context):
    run_number = event['data']['num']
    result = []
    last_num = 0
    current_num = 0
    current_run_number = 1
    while current_run_number <= run_number:
        if current_run_number == 1:
            result.append(1)
            last_num = current_num
            current_num = 1
        else:
            calculated_num = last_num + current_num
            result.append(calculated_num)
            last_num = current_num
            current_num = calculated_num
        current_run_number += 1
    return json.dumps(result)
